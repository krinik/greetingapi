FROM java:8
EXPOSE 8060
ADD /build/libs/GreetingAPI-0.0.1-SNAPSHOT.jar GreetingAPI-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","GreetingAPI-0.0.1-SNAPSHOT.jar"]