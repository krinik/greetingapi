package com.verizon.greetingapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.verizon.greetingapi.model.Greeting;

@RestController
public class GreetingController {

	@Autowired
	Greeting greeting; 
	@GetMapping(value="/nsrs/poc/greeting/v1/{name}")
	private Greeting greetUser(@PathVariable("name") String name) {
		greeting.setGreeting("Hi " + name);
		return greeting;
	}
}
