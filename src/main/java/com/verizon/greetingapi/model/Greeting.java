package com.verizon.greetingapi.model;

import org.springframework.stereotype.Component;

@Component
public class Greeting {

	private String greeting;

	public String getGreeting() {
		return greeting;
	}

	public void setGreeting(String greeting) {
		this.greeting = greeting;
	}
	
	
}
